
import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { render, cleanup, userEvent,screen, fireEvent} from "@testing-library/react";
import { reducers } from "../../redux/reducers";
import Navbar from "../../globals/Navbar";
import Alerts from "../../globals/Alerts";
import { hover } from "@testing-library/user-event/dist/hover";


const initialState = {};

//for redux

const renderWithRedux = (
  component,
  { initialState, store = createStore(reducers, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{component}</Provider>),
    store,
  };
};

afterEach(cleanup);

//NEXTFLIX icon is displayed correctly
it("should display nextflix icon", () => {
  const { getByTestId } = renderWithRedux(<Navbar />);
  expect(getByTestId("navbar-icon")).toHaveTextContent("NEXTFLIX");
});


// on hovering NEXTFLIX icon cursor should change
it('hover navbar-icon', () => {

  const { getByTestId } = renderWithRedux(<Navbar />);
  hover(getByTestId("navbar-icon"))
  expect(getByTestId("navbar-icon")).toHaveStyle({
    'cursor': 'pointer'
  })
  });


//No error on Navbar loading
it("should not show any error message when the component is loaded", () => {
    renderWithRedux(<Alerts />)
    const alertElement = screen.queryByRole("alert");
    expect(alertElement).not.toBeInTheDocument();
});