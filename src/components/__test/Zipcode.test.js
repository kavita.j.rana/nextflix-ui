import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { render, cleanup, screen } from "@testing-library/react";
import { reducers } from "../../redux/reducers";
import Zipcode from "../Zipcode/Zipcode";
import userEvent from "@testing-library/user-event";
import { fireEvent } from "@testing-library/react";

const initialState = {};

const renderWithRedux = (
  component,
  { initialState, store = createStore(reducers, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{component}</Provider>),
    store,
  };
};

afterEach(cleanup);

it("render text input", () => {
  const { getByTestId } = renderWithRedux(<Zipcode />);
  expect(getByTestId("textbox")).toBeInTheDocument();
});

it("should display type of textbox", () => {
  const { getByTestId } = renderWithRedux(<Zipcode />);
  expect(getByTestId("textbox")).toHaveAttribute("type", "text");
});

it("should have placeholder 'Enter Zipcode'", () => {
  const { getByTestId } = renderWithRedux(<Zipcode />);
  expect(getByTestId("textbox")).toHaveAttribute(
    "placeholder",
    "Enter Zipcode"
  );
});

it("pass valid zipcode to test Zipcode input field", async () => {
  renderWithRedux(<Zipcode />);

  const inputEl = screen.getByTestId("textbox");
  userEvent.type(inputEl, "110092");
  expect(screen.getByTestId("textbox")).toHaveValue("110092");
  expect(screen.queryByTestId("find-button")).not.toBeDisabled();
});

it("pass invalid zipcode to test Zipcode input field", () => {
  renderWithRedux(<Zipcode />);

  const inputEl = screen.getByTestId("textbox");
  userEvent.type(inputEl, "invalidtext12$%---   ");
  expect(screen.getByTestId("textbox")).toHaveValue("invalidtext12$%---   ");
  expect(screen.queryByTestId("find-button")).toBeDisabled();
  userEvent.clear(inputEl);
  expect(screen.queryByTestId("find-button")).toBeDisabled();
});

// it('tooltip apperas as arrow',async () => {
//   renderWithRedux(<Zipcode />);

//   const tooltip = screen.queryByRole('tooltip');
//   expect(tooltip).not.toBeInTheDocument();

//   const item = screen.getByTestId('find-button');
//   userEvent.hover(item);
//   const tooltipAppears = screen.getByTestId('button-tooltip')
//   expect(tooltipAppears[0]).toHaveAttribute('aria-label',"Please enter valid zipcode.");

//   userEvent.unhover(item);
//   const tooltipDisappears = screen.getByTestId('button-tooltip')
//   expect(tooltipDisappears).not.toBeInTheDocument();
// });

it("Button shows text - Find Channels", () => {
  renderWithRedux(<Zipcode />);
  expect(screen.getByRole("button")).toHaveTextContent("Find Channels");
});

// it('onClick of button calls onFindChannels method',() => {
//  const onFindChannelsSpy=jest.fn();
//  renderWithRedux(<Zipcode onFindChannels={onFindChannelsSpy}/>);
//  const button=screen.getByRole('button');
//  fireEvent.click(button);
//  expect(onFindChannelsSpy).toHaveBeenCalled();
// })

// it("onClick of button calls onFindChannels method", () => {
//   const onFindChannelsSpy = jest.fn();
//   renderWithRedux(<Zipcode />);
//   const button = screen.getByRole("button");
//   fireEvent.click(button);
//   expect(Zipcode.onFindChannels).toHaveBeenCalledWith(onFindChannelsSpy);
//   // const wrapper = shallow(<Zipcode />);
//   //   const button = wrapper.find("button");
//   //   const instance = wrapper.instance();

//   //   instance.btnClick = jest.fn(instance.btnClick);

//   //   button.simulate("click");
//   //   expect(instance.btnClick).toHaveBeenCalled();
// });

//  let mockCreateUser = jest.fn();
//  mockUseCreateUserTask.mockImplementation(() => ({
//     mutate: mockCreateUser,
//   }));

// const { mutate: createUserTask } = mockUseCreateUserTask();
