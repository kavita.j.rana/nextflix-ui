import React from 'react'
import {render, cleanup} from '@testing-library/react'
import Channels from '../Channel/Channels';
import { createStore } from "redux";
import { Provider } from "react-redux";
import { reducers } from "../../redux/reducers";

const initialState = {};

//for redux
const renderWithRedux = (
  component,
  { initialState, store = createStore(reducers, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{component}</Provider>),
    store,
  };
};

afterEach(cleanup);
 

//Channel Table Snapshot Match test
it('should take a snapshot', () => {
    const { asFragment } = renderWithRedux(<Channels />)
    
    expect(asFragment(<Channels />)).toMatchSnapshot()
   });
