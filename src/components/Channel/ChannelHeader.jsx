import React from "react";
import { TableCell, TableHead, TableRow } from "@mui/material";

const ChannelHeader = ({ columns }) => {
  const tableRowSX = {
    "& th": {
      color: "white",
      backgroundColor: "var(--table-header-color)",
      paddingLeft: 5,
      paddingRight: 5,
    },
  };

  return (
    <TableHead>
      <TableRow sx={tableRowSX}>
        {columns.map((column) => (
          <TableCell
            key={column.id}
            align={column.align}
            sx={{
              minWidth: column.minWidth,
              background: "black",
              color: "white",
            }}
          >
            {column.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default ChannelHeader;
