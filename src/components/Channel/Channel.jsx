import React from "react";
import { TableCell, TableRow } from "@mui/material";
import CheckIcon from "@mui/icons-material/Check";
import StarIcon from "@mui/icons-material/Star";

const Channel = (props) => {
  const tableRowSX = {
    "& td": {
      backgroundColor: "var(--table-data-color)",
      paddingLeft: 5,
      paddingRight: 5,
    },
  };

  return (
    <TableRow
      sx={tableRowSX}
      hover
      role="checkbox"
      tabIndex={-1}
      key={props.row.code}
    >
      {props.columns.map((column) => {
        const value = props.row[column.id];
        const isPremiumPackage = props.row.premiumPackage;
        console.log("value : ");
        console.log(value);
        console.log("isPremiumPackage : ");
        console.log(isPremiumPackage);
        return (
          <TableCell key={column.id} align={column.align}>
            {typeof value === "boolean" ? (
              value && isPremiumPackage ? (
                <StarIcon />
              ) : value ? (
                <CheckIcon />
              ) : (
                ""
              )
            ) : (
              value
            )}
          </TableCell>
        );
      })}
    </TableRow>
  );
};

export default Channel;
