import React, { useEffect } from "react";
import { Alert, Box, Collapse, IconButton } from "@mui/material";
import { connect } from "react-redux";
import CloseIcon from "@mui/icons-material/Close";

const Alerts = (props) => {
  const [open, setOpen] = React.useState(true);
  console.log("alert props");
  console.log(props);

  useEffect(() => {
    if (props.alert !== null) {
      setOpen(true);
    }
  }, [props.alert]);

  const alertSX = {
    mb: 2,
  };

  return props.alert !== null ? (
    <Box sx={{ marginTop: "1rem" }}>
      <Collapse in={open}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          severity={props.alert.type}
          sx={alertSX}
        >
          {props.alert.message}
        </Alert>
      </Collapse>
    </Box>
  ) : (
    <></>
  );
};

const mapStateToProps = (state) => {
  console.log("Alert mapStateToProps");
  console.log(state);
  return {
    alert: state.alert,
  };
};

export default connect(mapStateToProps)(Alerts);
