import React from "react";
import Alerts from "./Alerts";
import "./Navbar.css";

const Navbar = (props) => {
  return (
    <nav className="nav-flex">
      <h1
        data-testid="navbar-icon"
        className="heading"
        style={{ cursor: "pointer" }}
      >
        NEXTFLIX
      </h1>
      <div className="alert-flex">
        <Alerts />
      </div>
    </nav>
  );
};

export default Navbar;
