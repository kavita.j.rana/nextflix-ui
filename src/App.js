import React from "react";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import Channels from "./components/Channel/Channels";
import Navbar from "./globals/Navbar";
import Zipcode from "./components/Zipcode/Zipcode";
import "./App.css";
import Footer from "./globals/Footer";

function App() {
  return (
    <Provider store={store}>
      <div className="app">
        <Navbar />
        <div className="content">
          <Zipcode />
          <Channels />
        </div>
        <Footer />
      </div>
    </Provider>
  );
}

export default App;
