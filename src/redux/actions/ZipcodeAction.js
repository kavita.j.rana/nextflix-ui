import NextflixApi from "../../apis/NextflixApi";
import { FETCH_ZIPCODE } from "../../constants/ActionTypes";
import { ERROR } from "../../constants/AlertTypes";
import { GET_ZIPCODE_END_POINT } from "../../constants/ApiConstants";
import { resetAlert, setAlert } from "./AlertAction";

export const fetchZipcode = (zipcodeId) => async (dispatch) => {
  console.log("inside action fetchZipcode");
  const data = await NextflixApi.get(`${GET_ZIPCODE_END_POINT}\\${zipcodeId}`)
    .then((response) => {
      dispatch(resetAlert());
      return response.data;
    })
    .catch((err) => {
      dispatch(setAlert(ERROR, err.response.data.message));
      return err.response.data;
    });

  console.log(data);
  dispatch({
    type: FETCH_ZIPCODE,
    payload: data,
  });
};
