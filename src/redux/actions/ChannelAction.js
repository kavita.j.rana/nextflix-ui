import NextflixApi from "../../apis/NextflixApi";
import { DOWNLOAD_CHANNEL } from "../../constants/ActionTypes";
import {
  GET_ZIPCODE_END_POINT,
  DOWNLOAD_CHANNEL_END_POINT,
} from "../../constants/ApiConstants";

export const downloadChannel = (zipcodeId) => async (dispatch) => {
  console.log("inside action downloadChannel");
  console.log(zipcodeId);
  const data = await NextflixApi.get(
    GET_ZIPCODE_END_POINT + `\\${zipcodeId}` + DOWNLOAD_CHANNEL_END_POINT
  )
    .then((response) => response.data)
    .catch((err) => err.response.data);

  console.log(data);
  dispatch({
    type: DOWNLOAD_CHANNEL,
    payload: data,
  });
};
