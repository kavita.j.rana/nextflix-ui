import { FETCH_ZIPCODE } from "../../constants/ActionTypes";

const initialState = {
  channels: [],
};

export const zipcodeReducer = (zipcode = initialState, action) => {
  switch (action.type) {
    case FETCH_ZIPCODE: {
      console.log(FETCH_ZIPCODE);
      return action.payload;
    }
    default:
      return zipcode;
  }
};
