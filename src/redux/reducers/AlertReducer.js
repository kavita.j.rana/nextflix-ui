import { RESET_ALERT, SET_ALERT } from "../../constants/ActionTypes";

export const alertReducer = (alert = null, action) => {
  switch (action.type) {
    case SET_ALERT: {
      console.log(SET_ALERT);
      return action.payload;
    }
    case RESET_ALERT: {
      console.log(RESET_ALERT);
      return action.payload;
    }
    default:
      return alert;
  }
};
