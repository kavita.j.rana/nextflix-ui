import { combineReducers } from "redux";
import { alertReducer } from "./AlertReducer";
import { channelReducer } from "./ChannelReducer";
import { zipcodeReducer } from "./ZipcodeReducer";

export const reducers = combineReducers({
  zipcode: zipcodeReducer,
  file: channelReducer,
  alert: alertReducer,
});
